#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int converter(int);

int main(){
    FILE *f = fopen("log.txt", "r");
    
    if(!f){
        printf("Erro ao ler o arquivo");
        return 1;
    }

    int caract;
    char binario[8] = {0};
    int count = 0;
    while((caract = fgetc(f)) != -1){
        if(caract == '\n') {
            count = 0; 
            int bin = atoi(binario);
            int decimal = converter(bin);
            printf("Binario: %d Decimal: %d\n",bin , decimal);
         
        }
        if(caract == ';') {
            continue;
        }
        binario[count++] = caract;
    }
    fclose(f);
}

/**
 * Usado para converter binario em decimal
 * Ex. int dec = converter(00110000)
 *    Retorno esperado: 48
 * @param Numero binario a ser convertido para decimal
 */
int converter(int binario) {
    int auxiliar = binario;
    int decimal = 0;
    int base = 0;
    while(auxiliar != 0) {
        binario = auxiliar % 10;
        decimal = decimal + binario * pow(2, base++);
        auxiliar = auxiliar / 10;
    }
    return decimal;
}